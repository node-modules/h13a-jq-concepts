let somethingThatIsCalculated = 2 + 2;

module.exports = {

  "layout": { name: 'breadthfirst', padding: 10 },

  // <<PUT YOUR SETTINGS HERE>>

  "nodes": [
    { data: { id: 'j', name: 'Extremely High Frequency' } },
    { data: { id: 'e', name: 'Meteorological Satellite Program' } },
    { data: { id: 'k', name: 'Fleet Satellite Communications System' } },
    { data: { id: 'g', name: 'Electro-Optical Deep Space Surveillance' } },
  ],

  "edges": [
    { data: { source: 'j', target: 'e' } },
    { data: { source: 'j', target: 'k' } },
    { data: { source: 'j', target: 'g' } },
    { data: { source: 'e', target: 'j' } },
    { data: { source: 'e', target: 'k' } },
    { data: { source: 'k', target: 'j' } },
    { data: { source: 'k', target: 'e' } },
    { data: { source: 'k', target: 'g' } },
    { data: { source: 'g', target: 'j' } },
  ],



};
