const debounce = require('lodash/debounce');

const packageJson = require("./package.json");
const packageJsonMain = require("../../package.json");

const widgetNamespace = packageJsonMain.name.toLowerCase().replace(/-/g, '');
const widgetName = packageJson.name.toLowerCase().replace(/-/g, '');
const widgetFullName = `${widgetNamespace}.${widgetName}`;

require("h13a-strategy3"); // Simple 1+2 Strategy, initialize and then display ... display ... display
const widgetIntegrationStrategy = $[widgetNamespace].h13astrategy3;

//HTML DATA
const cytoscape = require("cytoscape/dist/cytoscape.min.js")

const defaults = require("./defaults.es6");

const functions = {

  // <<YOUR FUNCTIONS HERE>>

  _xy: function (node) {
    // http://caniuse.com/#feat=getboundingclientrect
    let info = node.getBoundingClientRect();
    //console.log(info);
    return info.left + 'x' + info.top;
  },

  _smile: function (location) {
    /*‿*/
  }

}

const integration = $.extend({
  options: defaults,

  _destroy: function () {
    // The public destroy() method cleans up all common data, events, etc. and then delegates out to _destroy() for custom, widget-specific, cleanup.
    if (this.cyInstance) this.cyInstance.destroy();
    console.log(`${this.widgetFullName}/${this.uuid}: destructor executed.`)
  },

  // Step1 Notes
  // * Create UI
  // * Called once at start
  // * Call display manually
  _prepare: function () {

    let type = this.option('type');
    let location = this.option('location');

    this.contentNode = $(`<div class="card-block concept-position"></div>`);
    this.node = $(`<div id="cy${this.uuid}" class="innie-sm" style="height: ${this.option("graphHeight")||'640'}px; width: 100%;"></div>`);

    this.contentNode.append(this.node);
    this.element.append(this.contentNode);

    // SIZE MONITOR -- older browsers and custom browser may have size issues
    const trimIntervalID = setInterval(() => {
      try {
        var w = this.contentNode.innerWidth();
        var W = this.node.width();
        if (W > w) {
          this.node.width(w);
          if (this.debouncedResize) this.debouncedResize();
          //if ( this.cyInstance ) this.cyInstance.reset();
          // console.log("NOT OK", w, W);
        }
        else {
          //console.log("OK", w, W);
        }
      }
      catch (e) {console.info(e)}
    }, 300);
    this.destructors(() => clearInterval(trimIntervalID));
    // SIZE MONITOR

    // REPOSITION MONITOR -- older browsers and custom browser may have size issues
    this.lastPosition = this._xy(this.node.get(0));
    const repositionIntervalID = setInterval(() => {
      try {
        let thisPosition = this._xy(this.node.get(0));
        if (this.lastPosition !== thisPosition) {
          if (this.debouncedResize) this.debouncedResize();
          //console.log('POSITION CHANGED!', this.uuid )
        }
        this.lastPosition = thisPosition;
        ///console.log(thisPosition)
      }
      catch (e) {console.info(e)}
    }, 900);

    this.destructors(() => clearInterval(repositionIntervalID));
    // REPOSITION MONITOR

    this.display();

  },

  // Step2 Notes
  // * Display On Screen
  // * This will be called each time options finish changing
  // * Called after _prepare finishes
  // * Called each time options change.
  display: function () {

    if (this.cyInstance) {
      this.cyInstance.destroy();
    }

    this.cyInstance = cytoscape({
      container: this.node,
      boxSelectionEnabled: false,
      autounselectify: true,

      style: cytoscape.stylesheet()

        .selector('node')
        .css({
          'background-color': '12223d',
          'content': 'data(name)',
          'text-valign': 'center',
          'color': 'cyan',

          'text-outline-width': 4,
          'text-outline-color': '#12223d'
        })

        .selector('edge')
        .css({
          'width': '8',
          'line-color': 'rgb(2, 77, 68)',
          'target-arrow-shape': 'triangle'
        })

        .selector(':selected')
        .css({
          'background-color': 'green',
          'line-color': 'red',
          'target-arrow-color': 'black',
          'source-arrow-color': 'black'
        })
        .selector('.faded')
        .css({
          'opacity': 0.15,
          'line-color': 'green',
          'text-opacity': 0
        }),

      elements: {
        nodes: this.option("nodes"),
        edges: this.option("edges")
      },

      layout: this.option("layout")

    });

    // this.debouncedResize = debounce(this.cyInstance.resize.bind(this.cyInstance), 666);
    if (!this.debouncedResize) this.debouncedResize = debounce(() => {
      try {
        if (this) this.cyInstance.resize()
      }
      catch (e) {
        if (this) this.debouncedResize = null;
      }
    }, 1000, {
      'maxWait': 1000 * 60
    });

    this.cyInstance.on('tap', 'node', (e) => {

      var node = e.cyTarget;
      // $(document).trigger('terminal', {data:`echo --type warning "Warning ${ node.attr("name") } Link Down"`});
      var neighborhood = node.neighborhood().add(node);
      this.cyInstance.elements().addClass('faded');
      neighborhood.removeClass('faded');

    });

    this.cyInstance.on('click', (e) => {
      if (e.cyTarget === this.cyInstance) {

        this.cyInstance.elements().removeClass('faded');

        // $(document).trigger('terminal', {data:`echo --type danger "Phased Array Down, Early Warning System Acrivated."`});

      }
    });

  },

}, functions);

// Install Widget
$.widget(widgetFullName, widgetIntegrationStrategy, integration);
